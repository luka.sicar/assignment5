import { Component, Input } from '@angular/core';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent {

  constructor(private userService: UserService) { }

  @Input() users: string[];

  onSetToActive(id: number) {
    this.userService.sendToActive(id)
  }
}