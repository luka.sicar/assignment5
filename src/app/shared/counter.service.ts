export class CounterService {

    private actionCount: number = 0

    checkActionCount(): void {
        this.actionCount = this.actionCount + 1
        console.log('Action ' + this.actionCount)
    }
}