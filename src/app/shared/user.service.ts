import { Injectable } from "@angular/core";
import { CounterService } from "./counter.service";

@Injectable()
export class UserService {

    private activeUsers: string[] = ['Max', 'Anna'];
    private inactiveUsers: string[] = ['Chris', 'Manu'];

    constructor(private countService: CounterService) { }

    getActiveUsers(): string[] {
        return this.activeUsers
    }

    getInactiveUsers(): string[] {
        return this.inactiveUsers
    }

    sendToActive(id: number): void {
        this.activeUsers.push(this.inactiveUsers[id]);
        this.inactiveUsers.splice(id, 1);
        this.countService.checkActionCount()
    }

    sendToInactive(id: number): void {
        this.inactiveUsers.push(this.activeUsers[id]);
        this.activeUsers.splice(id, 1);
        this.countService.checkActionCount()
    }
}